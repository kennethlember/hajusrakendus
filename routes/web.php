<?php

use App\Http\Controllers\MapsController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\WeatherController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ShipsController;
use App\Http\Controllers\CommentsController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/weather', [WeatherController::class, 'index']);

Route::get('/blog', [BlogController::class, 'index']);
Route::delete('/blog/{id}', [BlogController::class, 'delete'])->name('delete.blog');
Route::get('/blog/{id}', [BlogController::class, 'show'])->name('edit.blog');
Route::put('/blog/{id}', [BlogController::class, 'update'])->name('update.blog');
Route::post('/blog/add', [BlogController::class, 'store'])->name('add.blog');
Route::post('/comment/{blog}', [CommentsController::class, 'store'])->name('add.comment');
Route::post('/comment-delete/{comment}', [CommentsController::class, 'delete'])->middleware('role:Admin');

Route::get('/maps', [MapsController::class, 'index']);
Route::post('/maps', [MapsController::class, 'store']);
Route::delete('/maps/{id}', [MapsController::class, 'delete']);
Route::get('/maps/markers/{id}', [MapsController::class, 'markerEdit']);
Route::put('/maps/markers/{id}', [MapsController::class, 'update']);

Route::get('/store', [StoreController::class, 'index'])->name('store');
Route::post('/cart', [StoreController::class, 'cartAdd'])->name('add.cart');
Route::get('/cart', [StoreController::class, 'CartList'])->name('cart.list');
Route::put('/cart/{id}', [StoreController::class, 'cartUpdate'])->name('update.cart');
Route::delete('/cart/{id}', [StoreController::class, 'destroy'])->name('delete.cart');
Route::get('/stripeCart', [StoreController::class, 'checkoutData'])->name('get.stripeCart');
Route::get('/success', [StoreController::class, 'success']);


Route::get('/ships', [ShipsController::class, 'index']);

require __DIR__ . '/auth.php';
