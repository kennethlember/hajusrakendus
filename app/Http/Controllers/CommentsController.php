<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Comments;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Blog $blog, Request $request)
    {
        $blog->comments()->create([
            "comment" => $request->comment
        ]);

        return redirect()->back();
    }

    public function delete(Comments $comment)
    {
        $comment->delete();
        return redirect()->back();
    }
}
