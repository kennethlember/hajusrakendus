<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Markers;
use Inertia\Inertia;

class ShipsController extends Controller
{
    public function index(Request $request)
    {
        // $data = Ships::all();
        return Inertia::render('Ships');
    }
}
