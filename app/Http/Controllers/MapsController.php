<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Markers;
use Inertia\Inertia;

class MapsController extends Controller
{
    public function index()
    {
        $data = Markers::all();
        return Inertia::render('Maps', ['data' => $data]);
    }

    public function store(Request $request)
    {
        // return $request->;
        Markers::create([
            'name' => $request->name,
            'latitude' => $request->lat,
            'longitude' => $request->long,
            'description' => $request->description,
        ]);
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        $marker = Markers::findOrFail($id);

        $marker->update($request->all());

        return redirect("/maps");
    }
    public function markerEdit($id)
    {
        $data = Markers::find($id);
        return Inertia::render('MarkerEdit', ['data' => $data]);
    }
    public function delete(Request $request, $id)
    {
        $marker = Markers::find($id);
        $marker->delete();
        return redirect()->back();
    }
}
